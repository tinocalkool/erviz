FROM ubuntu:trusty

# Install prerequisites
RUN apt-get update
RUN apt-get install -y wget unzip patch openjdk-7-jre

# Download
RUN cd /opt \
	&& wget http://slopjong.de/wp-content/2011/02/erviz-1.0.6-bin.zip \
	&& unzip erviz-1.0.6-bin.zip \
	&& rm -f erviz-1.0.6-bin.zip

# Install
RUN cd /opt/erviz-1.0.6/ \
	&& chmod u+x setup.sh \
	&& echo 1 | ./setup.sh

# Patch
RUN cd /opt/erviz-1.0.6/ \
	&& wget http://slopjong.de/wp-content/2011/02/erviz.patch.zip \
	&& unzip erviz.patch.zip \
	&& rm -f erviz.patch.zip \
	&& cd bin/ \
	&& patch -p1 -i ../erviz.patch \
	&& rm -f erviz.patch

VOLUME /workspace
WORKDIR /workspace

ENTRYPOINT [ "/opt/erviz-1.0.6/bin/erviz.sh" ]