#!/bin/bash
set -e

pushd `dirname $0` > /dev/null

image='tadams/erviz:1.0.6'
docker build -t $image . \
	&& docker login \
	&& docker push $image
